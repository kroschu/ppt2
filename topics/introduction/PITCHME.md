
##### Автор проекту [ТребаТак]()
![Володимир Коваленко](assets/img/vokov.jpg)
<div class="align-points">
	<i class="fa fa-user"></i> Володимир Коваленко > [резюме](https://treba.ml/resume/)<br/>
	<i class="fa fa-github"></i> [github.com/kroschu](https://github.com/kroschu)<br/>
	<i class="fa fa-globe"></i> [Блог](https://treba.ml/blog/)<br/>
</div>

+++
> Кожен день віддані, мислячі, щирі люди вносять свій внесок в те, щоб змусити Вас робити все за їх вказівками. Таку перешкоду складно подолати.  
[Філіп Б. Кросбі](https://en.m.wikipedia.org/wiki/Philip_B._Crosby)

+++

